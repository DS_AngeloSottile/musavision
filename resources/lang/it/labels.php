<?php

return 
[
    'create_form' =>
    [
        'movie_name'        =>'Nome film',
        'movie_code'        =>'Codice film',
        'movie_duration'    =>'Durata film',
        'movie_cover'       =>'Cover film',
    ],
    'commons' =>
    [
        'movie_submit'      =>'Salva',
        'back'              =>'Indietro',
    ]
];