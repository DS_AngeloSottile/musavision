<?php

return 
[
    'create_form' =>
    [
        'movie_name'                    =>'Movie Name',
        'category'                      =>'Categoria',
        'movie_code'                    =>'Movie Code',
        'movie_duration'                =>'Movie Duration',
        'movie_cover'                   =>'Movie cover',
        'create_movie_form_success'     =>'Film create successful',
    ],
    'commons' =>
    [
        'movie_submit'          =>'Save',
        'back'                  =>'Back',
        'update'                =>'Update',
    ],
    'edit_form' =>
    [
        'edit_movie_form_success'           =>  'Edit successful',
        'edit_movie_form_error'             =>  'Edit error',
        'movie_not_found'                   =>  'Movie not found',
    ],
    'delete_form' =>
    [
        'delete_movie_form_success'           =>  'Delete successful',
        'delete_movie_form_error'             =>  'Delete error',
    ],
    
];