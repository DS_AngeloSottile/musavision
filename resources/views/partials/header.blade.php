<header id="header" class="bg-dark-blue">
        <div id="logo">
            <img  style=" width: auto; height:100%;" src="{{ asset('./immagini/logo.png') }}" alt="">
        </div>
        <nav id="main-menu">
            <ul>
                <li>
                    <div class="w-full h-full dt"><div class="dtc va-m"><i class="fas fa-film"></i><a href="/">Film</a></div></div>
                </li>
                <li>
                    <div class="w-full h-full dt"><div class="dtc va-m"><div class="separator"></div></div></div>
                </li>
                <li>
                    <div class="w-full h-full dt"><div class="dtc va-m"><i class="fas fa-film"></i><a href="/">Sale</a></div></div>
                </li>
                <li>
                    <div class="w-full h-full dt"><div class="dtc va-m"><div class="separator"></div></div></div>
                </li>
                <li>
                    <div class="w-full h-full dt"><div class="dtc va-m"><i class="fas fa-film"></i><a href="/">Prenotazioni</a></div></div>
                </li>
                <li>
                    <div class="w-full h-full dt"><div class="dtc va-m"><div class="separator"></div></div></div>
                </li>
                <li>
                    <div class="w-full h-full dt"><div class="dtc va-m"><i class="fas fa-film"></i><a href="/">Contatti</a></div></div>
                </li>

            </ul>
        </nav>
        <div id="secondary-menu" class="bg-dark-blue">
            <ul>
                <li>
                    <div class="w-full h-full dt"><div class="dtc va-m">
                    <!-- così levi il login al login -->
                        @guest
                            <a href="{{route( 'login') }}"class="mv-header-btn">login</a>
                        @else
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();"
                                                class="mv-header-btn">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        @endguest
                    <!-- così levi il login al login -->
                    </div></div><!-- la schermata di login è quella predefinita di Larael -->
                </li>
                <li>
                    <div class="w-full h-full dt"><div class="dtc va-m"><div class="separator"></div></div></div>
                </li>
                <li>
                    <div class="w-full h-full dt"><div class="dtc va-m"><i class="fas fa-film"></i></div></div>
                </li>
            </ul>
        </div>
    </header>
