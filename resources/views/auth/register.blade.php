@extends('layouts.app')

@section( 'styles' )
    <link rel="stylesheet" href="{{ asset('css/auth.css') }}" type="text/css">
@endsection

@section('content')
<div class="dt h-full w-full">
    <div class="dtc va-m">
        <div class="container">
            <div class="row ">
                <div class="col-xs-10 col-xs-offest-1 col-md-6 col-md-offset-3">
                    <form method="POST" action="{{ route('register') }}">
                        <fieldset>
                            <legend>
                                Register to MusaVision
                            </legend>
                            <div class="form-group row @error('name') has-error @enderror">
                                <label for="name" class="col-xs-12 control-label ">{{ __('Name') }}</label>

                                <div class="col-xs-12">
                                    <input id="name" type="text" class="form-control " name="name" value="{{ old('name') }}" >

                                    @error('name')
                                        <span class="invalid" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row @error('email') has-error @enderror">
                                <label for="email" class="col-xs-12 control-label">{{ __('E-Mail Address') }}</label>

                                <div class="col-xs-12">
                                    <input id="email" type="email" class="form-control " name="email" value="{{ old('email') }}" >

                                    @error('email')
                                        <span class="invalid" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row @error('password') has-error @enderror">
                                <label for="password" class="col-xs-12 control-label">{{ __('Password') }}</label>

                                <div class="col-xs-12">
                                    <input id="password" type="password" class="form-control " name="password" >

                                    @error('password')
                                        <span class="invalid" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm" class="col-xs-12 control-label">{{ __('Confirm Password') }}</label>

                                <div class="col-xs-12">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" >
                                </div>
                            </div>

                            <div class="form-group row ">
                                <div class="col-xs-12 text-center">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>
                        </fieldset>
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
