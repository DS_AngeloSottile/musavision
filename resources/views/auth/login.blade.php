@extends('layouts.app')

@section('content')

    @section( 'styles' )
        <link rel="stylesheet" href="{{ asset('css/auth.css') }}" type="text/css">
    @endsection
    <div class="dt h-full w-full">
        <div class="dtc va-m">
            <div class="container">
                <div class="row ">
                    <div class="col-xs-10 col-xs-offest-1 col-md-6 col-md-offset-3">
                    @include('components.message')
                        <form method="POST" action="{{ route('login') }}">
                            <fieldset>
                                <legend>
                                {{ __('Login to MusaVision') }}
                                </legend>

                                <div class="form-group row @error('email') has-error @enderror">
                                    <label for="email" class="col-xs-12 control-label">{{ __('E-Mail Address') }}</label>

                                    <div class="col-xs-12">
                                        <input id="email" type="email" class="form-control " name="email" value="{{ old('email') }}" >

                                        @error('email')
                                            <span class="invalid" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row @error('password') has-error @enderror">
                                    <label for="password" class="col-xs-12 control-label">{{ __('Password') }}</label>

                                    <div class="col-xs-12">
                                        <input id="password" type="password" class="form-control " name="password" >

                                        @error('password')
                                            <span class="invalid" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row ">
                                    <div class="col-xs-12 text-center">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                            <label class="form-check-label" for="remember">
                                                {{ __('Remember Me') }}
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row ">
                                    <div class="col-xs-12 text-center">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Login') }}
                                        </button>
                                    </div>
                                </div>

                                <div class="form-group row ">
                                    <div class="col-xs-12 text-center">
                                        @if (Route::has('password.request'))
                                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                                {{ __('Forgot Your Password?') }}
                                            </a>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row ">
                                    <div class="col-xs-12 text-center">
                                        <a  href="{{ route('register') }}">
                                            {{ __('Register to MusaVision') }}
                                        </a>
                                    </div>
                                </div>

                            </fieldset>
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
