@extends('layouts.admin-app')

@section('content')
    <div class="container">
        <div class="row">
            @include('components.message')
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading"> List Utenti</div>
                    <div class="panel-body"> <!-- Esiste un altro ciclo per laravel ovvero il forelse, non mi piace LEZIONE 26 40MIN -->
                    <div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="@if( $current_tab == 1 ) active @endif">
                                <a href="#status-1" aria-controls="status-1" role="tab" data-toggle="tab"> Utenti Attivi</a>
                            </li>
                            <li role="presentation" class="@if( $current_tab == 2 ) active @endif">
                                <a href="#status-0" aria-controls="status-0" role="tab" data-toggle="tab"> Utenti Non Attivi</a>
                            </li>
                        </ul>

                        <!-- Tab panes UTENTI ATTIVI-->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane @if( $current_tab == 1 ) active @endif" id="status-1">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <a class="btn btn-primary" href="{{ route( 'admin.user.index',['sort' =>'asc']) }}">
                                            <span class="glyphicon glyphicon-sort-by-alphabet" aria-hidden="true"></span>
                                        </a>
                                        <a class="btn btn-primary" href="{{ route( 'admin.user.index',['sort' =>'desc']) }}">
                                            <span class="glyphicon glyphicon-sort-by-alphabet-alt" aria-hidden="true"></span>
                                        </a>
                                    </div>
                                    <div class="col-xs-6">
                                    </div>
                                </div>
                                @if(count($users) > 0 )
                                    <ul class="list-group">
                                        @foreach($users['active'] as $user)
                                            @include('admin.users.components.users-list-item',['user => $user'])
                                        @endforeach
                                    </ul>
                                    <div class="text-center">
                                        {{$users['active']->appends( [ 'tab' => 1 ] )->links('admin.users.components.users-list-paginator')}} <!-- php artisan vendor:publish --tag=laravel-pagination in questo modo crei le pagine per il paginatore nella cartella vendor ch esi trova dentro le veiw, in questo modo hai il tag HTML del paginatore e puoi modificarlo a tuo piacimento-->
                                    </div>
                            </div>
                                @else
                                    <div class="alert alert-warning" role="alert">
                                        Spiacenti non ci sono utenti nella base dati,
                                    </div>
                                @endif
                            <!-- Tab panes UTENTI ATTIVI-->

                            <!-- Tab panes UTENTI NON ATTIVI-->
                            <div role="tabpanel" class="tab-pane @if( $current_tab == 2 ) active @endif" id="status-0">
                            <div class="row">
                                <div class="col-xs-6">
                                    <a class="btn btn-primary" href="{{ route( 'admin.user.index',['sort' =>'asc']) }}">
                                        <span class="glyphicon glyphicon-sort-by-alphabet" aria-hidden="true"></span>
                                    </a>
                                    <a class="btn btn-primary" href="{{ route( 'admin.user.index',['sort' =>'desc']) }}">
                                        <span class="glyphicon glyphicon-sort-by-alphabet-alt" aria-hidden="true"></span>
                                    </a>
                                </div>
                                <div class="col-xs-6">

                                </div>
                            </div>
                            @if(count($users) > 0 )
                                <ul class="list-group">
                                    @foreach($users['not_active'] as $user)
                                        @include('admin.users.components.users-list-item',['user => $user'])
                                    @endforeach
                                </ul>
                                <div class="text-center">
                                    {{$users['not_active']->appends( [ 'tab' => 2 ] )->links('admin.users.components.users-list-paginator')}}
                                </div>
                            </div>
                            @else
                                <div class="alert alert-warning" role="alert">
                                    Spiacenti non ci sono utenti nella base dati,
                                </div>
                            @endif
                            <!-- Tab panes UTENTI NON ATTIVI-->
                        </div>

                    </div>

                    </div>
                    <div class="panel-footer">
                        <a href="" class="btn btn-danger"> Indietro </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
