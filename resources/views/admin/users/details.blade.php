@extends('layouts.admin-app')

@section('content')
    <div class="container">
        <div class="row">
            @include('components.message')
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading"> {{$user->name}}</div>
                    <div class="panel-body">

                        @if( $user->details == null )

                            'L'utnte non ha dettagli'

                        @else
                            <strong> Age </strong>
                            {{ $user->details->age }}
                            {{ $user->details->birth_date }}

                            <strong> newsletter </strong>
                            {{$user->details->newsletter}}

                        @endif
                    </div>
                    <div class="panel-footer"> <a href="{{ route('admin.user.index') }}" class="btn btn-danger"> Indietro </a> </div>
                </div>
            </div>
        </div>
    </div>
@endsection
