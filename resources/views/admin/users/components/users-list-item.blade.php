
<li class="list-group-item ">
    <div class="row">
        <div class="col-xs-12 col-md-8">
            {{$user->name}}
            <br>
            {{$user->email}}
        </div>
        <div class="text-right col-xs-12 col-md-4">

            <a href=" " class="btn btn-primary">
                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
            </a>

            <a href="{{route('admin.user.show', $user->id)}}" class="btn btn-success">
                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            </a>

            @if($user->trashed())
                <a href="{{ route('admin.user.restore',$user->id) }}" class="btn btn-info">
                    <span class="glyphicon glyphicon-open" aria-hidden="true"></span>
                </a>

                <form style=" display:inline;" method="POST" action="{{route('admin.user.destroy', $user->id )}}">

                    @csrf
                    @method('delete')

                    <button type="submit"  class="btn btn-danger" name="delete_action" value="d">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>

                </form>
            @else
            <form style=" display:inline;" method="POST" action="{{route('admin.user.destroy', $user->id )}}">
                @csrf
                @method('delete')
                <button type="submit"  class="btn btn-danger" name="delete_action" value="t">
                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
            </form>
            @endif
        </div>
    </div>
</li>
