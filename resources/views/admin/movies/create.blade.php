@extends('layouts.admin-app')
@section('content')
            <div class="container">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <!-- @include('components.message') -->
                <div class="row">
                    <div class="col-xs-12">
                        <form method="POST" action="{{route('admin.movies.store')}}" enctype="multipart/form-data"> <!-- enctype="multipart/form-data" per fargli recepire i file -->
                            @csrf

                            <div class="form-group @error('name') has-error @enderror">
                                <label class="control-label" for="movie-title">{{ __('labels.create_form.movie_name') }}</label>
                                <input type="text" class="form-control" id="movie-title" name="name" value="{{old('name') ? old('name') : ' '}}">
                                @if($errors->has('name'))
                                    <span class="help-block">{{$errors->first('name')}}</span>
                                @endif
                            </div>

                            <div class="form-group  @error('code') has-error @enderror">
                                <label class="control-label" for="movie-code">{{ __('labels.create_form.movie_code') }}</label>
                                <input type="text" class="form-control" id="movie-code" name="code" value="{{old('code') ? old('code') : ' '}}">
                                @if($errors->has('code'))
                                    <span class="help-block">{{$errors->first('code')}}</span>
                                @endif
                            </div>
                            <div class="form-group  @error('duration') has-error @enderror">
                                <label class="control-label" for="movie-duration">{{ __('labels.create_form.movie_duration') }}</label>
                                <input type="text" class="form-control" id="movie-duration" name="duration" value="{{old('duration') ? old('duration') : ' '}}">
                                @if($errors->has('duration'))
                                    <span class="help-block">{{$errors->first('duration')}}</span>
                                @endif
                            </div>

                            <div class="form-group  @error('cover_path') has-error @enderror">
                                <label class="control-label" for="movie-cover">{{ __('labels.create_form.movie_cover') }}</label>
                                <input type="file" class="form-control" id="movie-cover" name="cover_path" value="{{old('cover_path') ? old('cover_path') : ' '}}">
                                <!-- <input type="text" class="form-control" id="movie-cover" name="cover_path" value="{{old('cover_path') ? old('cover_path') : ' '}}"> -->
                                @if($errors->has('cover_path'))
                                    <span class="help-block">{{$errors->first('cover_path')}}</span>
                                @endif
                            </div>
                            <!-- Se te ne serve solo 1
                            <div class="form-group @error('category_id') has-error @enderror">
                                <label class="control-label" for="category_id">{{ __('labels.create_form.category') }}</label>
                                     Primo parametro name del form,secondo parametro array chiave valore, terzo parametro fregatene,c'era messo null
                                {{ Form::select('category_id', $categories_select,null,['placeholder'=>'Selezionare una categorie','class'=>'form-control', 'id'=>'category_id'])}}
                                @if($errors->has('category_id'))
                                    <span class="help-block">{{$errors->first('category_id')}}</span>
                                @endif
                            </div>
                            -->

                            <!-- se te ne servono di piu -->
                            <div class="form-group @error('categories') has-error @enderror">
                                <label class="control-label" for="categories">{{ __('labels.create_form.category') }}</label>
                                <div>

                                    @foreach ($categories_select as $key =>$value)
                                        <div class="checkbox-inline">
                                            <label>
                                                <input  type="checkbox"
                                                        name = "categories[]"
                                                        value = "{{$key}}"
                                                        @if( old('categories') && in_array( $key, old('categories') )) checked @endif >
                                                        {{$value}}
                                            </label>
                                        </div>
                                    @endforeach

                                    @if($errors->has('categories'))
                                        <span class="help-block">{{$errors->first('categories')}}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-default"> {{ __('labels.commons.movie_submit') }} </button>
                                <a href="{{url('admin-movies-managment/movies')}}" class="btn btn-danger"> {{ __('labels.commons.back') }} </a>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
@endsection
