@extends('layouts.admin-app')

@section('content')
    <div class="container">
        <div class="row">
            @include('components.message')
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading"> {{$movie->name}}</div>
                    <div class="panel-body">
                    @foreach( $movie->categories as $category )
                    <li class="list-group-item">
                        <strong> {{$category->name}} </strong> </br>
                    </li>
                    @endforeach
                    </div>
                    <div class="panel-footer">
                        <a href="{{ route('admin.movies.index') }}" class="btn btn-danger"> Indietro </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
