@extends('layouts.admin-app')

@section('content')
    <div class="container">
        <div class="row">
            @include('components.message') {{--}} IMPORTANT{{--}}
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading"> List Film</div>
                    <div class="panel-body"> <!-- Esiste un altro ciclo per laravel ovvero il forelse, non mi piace LEZIONE 26 40MIN -->
                    @if(count($movies) > 0 )
                        <ul class="list-group">
                            @foreach($movies as $movie)
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="media">
                                                <div class="media-left">
                                                    <img class="media-object" width="150" src="{{ asset( 'storage/' . $movie->cover_path ) }}" alt="...">
                                                </div>
                                                <div class="media-body">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-8">
                                                            {{$movie->name}} <span class="badge"> {{ $movie->categories_count}}</span>
                                                        </div> <!-- [ 'movie' => $movie->id ] -->
                                                        <div class="col-xs-12 col-md-4 text-right">

                                                            <a href="{{ route( 'admin.movies.edit', $movie->id ) }}" class="btn btn-primary movies-list-action">
                                                                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                                            </a>

                                                            <a href="{{route('admin.movies.show', $movie->id)}}" class="btn btn-success movies-list-action">
                                                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                                            </a>
                                                            <!-- Delete funziona solo con il form-->
                                                            <form style=" display:inline;" method="POST" action="{{route('admin.movies.destroy', $movie->id )}}"> <!-- 'movie' lo hai passato con il compact  -->
                                                                @csrf
                                                                @method('delete')
                                                                <button type="submit"  class="btn btn-danger movies-list-action">
                                                                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                                </button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    @else
                        <div class="alert alert-warning" role="alert">
                            Spiacenti non ci sono film nella base dati,
                            <a href=""> clicca qua per inserire il tup primo film </a>
                        </div>
                    @endif
                    </div>
                    <div class="panel-footer">
                        <!-- se ti stai chidendo perchè hanno lo stesso url è perchè esso cambia a secondo della richiesta -->
                        <a href="{{url('admin-movies-managment/movies/create')}}" class="btn btn-primary"> Aggiungi un film </a>
                        <a href="" class="btn btn-danger"> Indietro </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
