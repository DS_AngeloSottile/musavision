@extends('layouts.admin-app')
@section('content')
            <div class="container">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @include('components.message')
                <div class="row">
                    <div class="col-xs-12"> <!-- [ 'movie' => $movie->id ] -->
                        <form method="POST" action="{{route('admin.movies.update', $movie->id )}}"> <!-- 'movie' lo hai passato con il compact  -->
                            @csrf
                            @method('patch')
                            <div class="form-group @error('name') has-error @enderror">
                                <label class="control-label" for="movie-title">{{ __('labels.create_form.movie_name') }}</label>
                                <input type="text" class="form-control" id="movie-title" name="name" value="{{old('name') ? old('name') : $movie->name}}">
                                @if($errors->has('name'))
                                    <span class="help-block">{{$errors->first('name')}}</span>
                                @endif
                            </div>

                            <div class="form-group  @error('code') has-error @enderror">
                                <label class="control-label" for="movie-code">{{ __('labels.create_form.movie_code') }}</label>
                                <input readonly disabled type="text" class="form-control" id="movie-code" name="code" value="{{ $movie->code }}">
                                @if($errors->has('code'))
                                    <span class="help-block">{{$errors->first('code')}}</span>
                                @endif
                            </div>
                            <div class="form-group  @error('duration') has-error @enderror">
                                <label class="control-label" for="movie-duration">{{ __('labels.create_form.movie_duration') }}</label>
                                <input type="text" class="form-control" id="movie-duration" name="duration" value="{{old('duration') ? old('duration') : $movie->duration}}">
                                @if($errors->has('duration'))
                                    <span class="help-block">{{$errors->first('duration')}}</span>
                                @endif
                            </div>

                            <div class="form-group  @error('cover_path') has-error @enderror">
                                <label class="control-label" for="movie-cover">{{ __('labels.create_form.movie_cover') }}</label>
                                <input type="text" class="form-control" id="movie-cover" name="cover_path" value="{{old('cover_path') ? old('cover_path') : $movie->cover_path}}">
                                @if($errors->has('cover_path'))
                                    <span class="help-block">{{$errors->first('cover_path')}}</span>
                                @endif
                            </div>

                            <div class="form-group @error('categories') has-error @enderror">
                                <label class="control-label" for="categories">{{ __('labels.create_form.category') }}</label>
                                <div>
                                    @foreach ($categories as $category)
                                        @if(old('categories'))
                                            <div class="checkbox-inline">
                                                <label>
                                                    <input  type="checkbox"
                                                            name = "categories[]"
                                                            value = "{{$category->id}}"
                                                            @if( in_array( $category->id, old('categories') )) checked @endif >
                                                            {{$category->name}}
                                                </label>
                                            </div>
                                        @else

                                            @php
                                                $found = false;
                                                $checked ='';
                                            @endphp

                                            @foreach ($movie->categories as $movie_category)
                                                @if($movie_category->id === $category->id && !$found)
                                                    @php
                                                        $found = true;
                                                        $checked ='checked';
                                                    @endphp
                                                @endif
                                            @endforeach

                                                <div class="checkbox-inline">
                                                    <label>
                                                        <input  type="checkbox"
                                                                name = "categories[]"
                                                                value = "{{$category->id}}"
                                                                {{$checked}}> {{$category->name}}
                                                    </label>
                                                </div>

                                        @endif
                                    @endforeach
                                    @if($errors->has('categories'))
                                        <span class="help-block">{{$errors->first('categories')}}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-default"> {{ __('labels.commons.update') }} </button>
                                <a href="{{url('admin-movies-managment/movies')}}" class="btn btn-danger"> {{ __('labels.commons.back') }} </a>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
@endsection
