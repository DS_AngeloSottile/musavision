@extends('layouts.app')

@section('content')

    @section( 'styles' )
        <link rel="stylesheet" href="{{ asset('css/auth.css') }}" type="text/css">
    @endsection
    <div class="dt h-full w-full">
        <div class="dtc va-m">
            <div class="container">
                <div class="row ">
                    <div class="col-xs-10 col-xs-offest-1 col-md-6 col-md-offset-3">
                    @include('components.message')
                        <form method="POST" action="{{ route('update-profile') }}">
                            <fieldset>
                                @csrf
                                @method('patch')
                                <div class="form-group row @error('name') has-error @enderror">
                                    <label for="name" class="col-xs-12 control-label">{{ __('Name') }}</label>

                                    <div class="col-xs-12">
                                    <input id="name" type="test" class="form-control " name="name" value="{{old('name') ? old('name') : $user->name }}" >

                                        @error('name')
                                            <span class="invalid" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row ">
                                    <div class="col-xs-12 text-center">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Save') }}
                                        </button>
                                    </div>
                                </div>

                            </fieldset>
                            @csrf
                        </form>
                    </div>
                    <div class="col-xs-12 text-center">
                        <form style=" display:inline;" method="POST" action="{{route('delete-profile' )}}">
                            @csrf
                            @method('delete')
                            <button type="submit"  class="btn btn-danger" name="delete_action" >
                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
