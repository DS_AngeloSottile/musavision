@extends('layouts.app')

{{$pageTitle}}
@section('content')
    {{-- Allora,hai richiamato la pagina gallery e dnetro di esssa c'è uno $slot il suo ocntenuto
                    è riempito dal questo <strong> che hai messo --}}
    @component('components.gallery',['test' =>'Valore di test'])
        <strong>ciao</strong>
    @endcomponent
    <span style="color: #fff">
    </span>
    <section id="movies" class="padded-section">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center"> <!-- BOOTSTRAP difficile da comprendere, serve per le tabelle -->
                        <h2 class="section-title">
                            FILMS
                        </h2>
                    </div>
                </div>
                <div id="movies-wall" class="row"> <!-- BOOTSTRAP difficile da comprendere, serve per le tabelle -->
                    <article class="single-movie-block col-xs-12 col-sm-8 col-sm-offset-2  col-md-6 col-md-offset-0 col-lg-3">
                        <div class="single-movie-poster" style="background-image: url('./immagini/thumbs/1.jpg')"></div>
                        <div class="single-movie-data text-center"> <!-- BOOTSTRAP text-center -->
                            <h4>TITOLO</h4>
                            <a href="" class="btn2 mv-btn-blue  single-all-info-cta">INFO FILM</a> <!-- BOOTSTRAP hà già dei bottoni -->
                        </div>

                    </article>
                    <article class="single-movie-block col-xs-12 col-sm-8 col-sm-offset-2  col-md-6 col-md-offset-0 col-lg-3">
                        <div class="single-movie-poster" style="background-image: url('./immagini/thumbs/2.jpg')"></div>
                        <div class="single-movie-data text-center">
                            <h4>TITOLO</h4>
                            <a href="" class="btn2 mv-btn-blue single-all-info-cta">INFO FILM</a>
                        </div>
                    </article>
                    <article class="single-movie-block col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-0 col-lg-3">
                        <div class="single-movie-poster" style="background-image: url('./immagini/thumbs/3.jpg')"></div>
                        <div class="single-movie-data text-center">
                            <h4>TITOLO</h4>
                            <a href="" class="btn2 mv-btn-blue single-all-info-cta">INFO FILM</a>
                        </div>
                    </article>
                    <article class="single-movie-block col-xs-12 col-sm-8 col-sm-offset-2  col-md-6 col-md-offset-0 col-lg-3">
                        <div class="single-movie-poster" style="background-image: url('./immagini/thumbs/4.jpg')"></div>
                        <div class="single-movie-data text-center">
                            <h4>TITOLO</h4>
                            <a href="" class="btn2 mv-btn-blue single-all-info-cta">INFO FILM</a>
                        </div>
                    </article>
                    <article class="single-movie-block col-xs-12 col-sm-8 col-sm-offset-2  col-md-6 col-md-offset-0 col-lg-3">
                        <div class="single-movie-poster" style="background-image: url('./immagini/thumbs/5.jpg')"></div>
                       <div class="single-movie-data text-center">
                            <h4>TITOLO</h4>
                            <a href="" class="btn2 mv-btn-blue single-all-info-cta">INFO FILM</a>
                        </div>
                    </article>
                    <article class="single-movie-block col-xs-12 col-sm-8 col-sm-offset-2  col-md-6 col-md-offset-0 col-lg-3">
                        <div class="single-movie-poster" style="background-image: url('./immagini/thumbs/6.jpg')"></div>
                        <div class="single-movie-data text-center">
                            <h4>TITOLO</h4>
                            <a href="" class="btn2 mv-btn-blue single-all-info-cta">INFO FILM</a>
                        </div>
                    </article>
                    <article class="single-movie-block col-xs-12 col-sm-8 col-sm-offset-2  col-md-6 col-md-offset-0 col-lg-3">
                        <div class="single-movie-poster" style="background-image: url('./immagini/thumbs/7.jpg')"></div>
                        <div class="single-movie-data text-center">
                            <h4>TITOLO</h4>
                            <a href="" class="btn2 mv-btn-blue single-all-info-cta">INFO FILM</a>
                        </div>
                    </article>
                    <article class="single-movie-block col-xs-12 col-sm-8 col-sm-offset-2  col-md-6 col-md-offset-0 col-lg-3">
                        <div class="single-movie-poster" style="background-image: url('./immagini/thumbs/8.jpg')"></div>
                        <div class="single-movie-data text-center">
                            <h4>TITOLO</h4>
                            <a href="" class="btn2 mv-btn-blue single-all-info-cta">INFO FILM</a>
                        </div>
                    </article>
                </div>
            </div>
        </section>
       <section id="halls" class="padded-section">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <h2 class="section-title">
                            SALE
                        </h2>
                    </div>
                </div>
                <div id="halls-wall" class="row">
                    <article class="single-hall-block col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-0 col-lg-4">
                        <div class="single-hall-img" style="background-image: url('./immagini/halls/sala-1.png')"></div>
                        <div class="hall-data text-center">
                            <h4>SALA 1</h4>
                            <a href="" class="btn2 mv-btn-purple  single-all-info-cta">PRENOTA</a>
                        </div>

                    </article>
                    <article class="single-hall-block col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-0 col-lg-4">
                        <div class="single-hall-img" style="background-image: url('./immagini/halls/sala-1.png')"></div>
                        <div class="hall-data text-center">
                            <h4>SALA 2</h4>
                            <a href="" class="btn2 mv-btn-purple  single-all-info-cta">PRENOTA</a>
                        </div>
                    </article>
                    <article class="single-hall-block col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-0 col-lg-4">
                        <div class="single-hall-img" style="background-image: url('./immagini/halls/sala-1.png')"></div>
                        <div class="hall-data text-center">
                            <h4>SALA 3</h4>
                            <a href="" class="btn2 mv-btn-purple  single-all-info-cta">PRENOTA</a>
                        </div>
                    </article>
                    <article class="single-hall-block col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-0 col-lg-4">
                        <div class="single-hall-img" style="background-image: url('./immagini/halls/sala-1.png')"></div>
                        <div class="hall-data text-center">
                            <h4>SALA 4</h4>
                            <a href="" class="btn2 mv-btn-purple  single-all-info-cta">PRENOTA</a>
                        </div>
                    </article>
                    <article class="single-hall-block col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-0 col-lg-4">
                        <div class="single-hall-img" style="background-image: url('./immagini/halls/sala-1.png')"></div>
                        <div class="hall-data text-center">
                            <h4>SALA 5</h4>
                            <a href="" class="btn2 mv-btn-purple  single-all-info-cta">PRENOTA</a>
                        </div>
                    </article>
                    <article class="single-hall-block col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-0 col-lg-4">
                        <div class="single-hall-img" style="background-image: url('./immagini/halls/sala-1.png')"></div>
                       <div class="hall-data text-center">
                            <h4>SALA 6</h4>
                            <a href="" class="btn2 mv-btn-purple  single-all-info-cta">PRENOTA</a>
                        </div>
                    </article>
                    <article class="single-hall-block col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-0 col-lg-4">
                        <div class="single-hall-img" style="background-image: url('./immagini/halls/sala-1.png')"></div>
                       <div class="hall-data text-center">
                            <h4>SALA 7</h4>
                            <a href="" class="btn2 mv-btn-purple  single-all-info-cta">PRENOTA</a>
                        </div>
                    </article>
                    <article class="single-hall-block col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-0 col-lg-4">
                        <div class="single-hall-img" style="background-image: url('./immagini/halls/sala-1.png')"></div>
                        <div class="hall-data text-center">
                            <h4>SALA 8</h4>
                            <a href="" class="btn2 mv-btn-purple  single-all-info-cta">PRENOTA</a>
                        </div>
                    </article>
                </div>
            </div>
         </section>

    <section id="book-now">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <div class="section-title-opaque-container">
                        <h2 class="section-title text-uppercase opaque"> <!-- BOOTSTRAP text-uppercase -->
                            prenotazione
                        </h2>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="book-now-form-container text-center">
                               <div class="row">
                                    <div class="col-md-6 hidden-xs hidden-sm"> <!-- BOOTSTRAP questi servono per le risoluzioni dei dispositivi -->
                                        <img src="./immagini/halls/sala-1.png">
                                    </div>
                                    <!-- BOOTSTRAP form default -->
                                    <div class="col-xs-12 col-md-6 ">
                                        <div class="c-verti">
                                            <div class="row book-now-form-row">
                                                <div class="col-xs-1">
                                                    <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </div>
                                                <div class="col-xs-11">
                                                    <div class="form-group">
                                                        <select  class="form-control" name="movie_id">
                                                            <option value="">Scegli un film</option>
                                                            <option value="1">Frozen</option>
                                                            <option value="2">Tolo Tolo</option>
                                                            <option value="3">Movie 3</option>
                                                            <option value="4">Movie 4</option>
                                                            <option value="5">Movie 5</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row book-now-form-row">
                                                <div class="col-xs-1">
                                                    <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </div>
                                                <div class="col-xs-11">
                                                    <div class="form-group">
                                                        <select class="form-control" name="movie_id">
                                                            <option value="">Scegli un film</option>
                                                            <option value="1">Frozen</option>
                                                            <option value="2">Tolo Tolo</option>
                                                            <option value="3">Movie 3</option>
                                                            <option value="4">Movie 4</option>
                                                            <option value="5">Movie 5</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row book-now-form-row">
                                                <div class="col-xs-1">
                                                    <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                                                </div>
                                                <div class="col-xs-11">
                                                    <div class="form-group">
                                                        <select class="form-control" name="movie_id">
                                                            <option value="">Scegli un film</option>
                                                            <option value="1">Frozen</option>
                                                            <option value="2">Tolo Tolo</option>
                                                            <option value="3">Movie 3</option>
                                                            <option value="4">Movie 4</option>
                                                            <option value="5">Movie 5</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="search" class="btn2 mv-btn-purple btn-mv">cerca</div>
                                        </div>
                                    </div>
                                    <!-- BOOTSTRAP FROM DEFAULT -->
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    </main>
    <section>
        <div id="map" style="background-image: url('./immagini/acaso/immaginine/mappa.png');">

        </div>
    </section>
    <footer id="footer">
        <div class="container-fluid">
            <div class="row eq-col-row">
                <div class="col-xs-8 col-md-8 text-center pull-right">
                    <div id="footer-logo" style="background-image: url('./immagini/acaso/immaginine/musa-vision-logo.png')"></div>
                </div>
                <div class="col-xs-2 col-md-2 pull-left">
                    <div id="footer-social-container">
                        <a href="https://www.facebook.com/" target="blank"><i class="glyphicon glyphicon-search" aria-hidden="true"></i></a>
                        <a href="https://www.instagram.com/" target="blank"><i class="glyphicon glyphicon-search" aria-hidden="true"></i></a>
                    </div>
                </div>

                <div class="col-xs-2 col-md-2 text-right hidden-xs hidden-sm">
                    <div id="back-to-top">
                        <div class="dt w-full h-full">
                            <div class=" dtc va-m">
                                <i class="glyphicon glyphicon-eject" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
@endsection

@section('scripts')
    <script src="{{ asset('js/script.js') }}" defer ></script>
@endsection
