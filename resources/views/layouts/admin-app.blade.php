<!DOCTYPE html>

<html style=" margin: 0px;">
<head>


    <!-- JAVASCRIPT -->
    <script src="{{ asset('js/app.js') }}" defer ></script>
    <script src="{{ asset('js/vendors/bootstrap.js') }}" defer ></script>
    @yield('scripts')
    <!-- JAVASCRIPT -->


    <!-- CSS -->         <!-- con assets sei già dentro public -->
    <link rel="stylesheet" href="{{ asset('css/admin-app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/vendors/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/vendors/bootstrap-theme.css') }}">
    <!-- CSS -->




<title>{{ config('app.name', 'Laravel') }}</title> <!-- nell'env se intend istaccare il nome metti gli apici -->
</head>
<body style=" margin: 0px; font-family: 'Source Code Pro', monospace; ">

    @include('partials.admin-header')
    <!-- metodo di laravel per includere i pezzi di codice html  -->

    <main id="main">
        @yield('content')
    </main>
    <!-- metodo di laravel per includere i pezzi di codice in home.blade.php  -->



</body>
</html>
