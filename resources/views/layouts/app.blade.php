<!DOCTYPE html>

<html style=" margin: 0px;">
<head>

    <!-- JAVASCRIPT -->
    <script src="{{ asset('js/app.js') }}" defer ></script>
    <script src="{{ asset('js/vendors/bootstrap.js') }}" defer ></script>
    @yield('scripts')
    <!-- JAVASCRIPT -->


    <!-- CSS -->         <!-- con assets sei già dentro public -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/vendors/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/vendors/bootstrap-theme.css') }}">
    <!-- CSS -->

    <!-- FONTAWESOME -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" crossorigin="anonymous" referrerpolicy="no-referrer" />




<title>{{ config('app.name', 'Laravel') }}</title> <!-- nell'env se intend istaccare il nome metti gli apici -->
</head>
<body style=" margin: 0px; font-family: 'Source Code Pro', monospace; ">

    @include('partials.header')
    <!-- metodo di laravel per includere i pezzi di codice html  -->
    @yield('styles')
    <main id="main">
        @yield('content')
    </main>
    <!-- metodo di laravel per includere i pezzi di codice in home.blade.php  -->



</body>
</html>
