
{{ $slot }}
{{ $test }}
<section id="herogallery">
            <div id="galleryprimaria">
                <div class="gallerynav" id="gallerynavprev" data-action="p"></div>
                <div class="gallerynav" id="gallerynavnext" data-action="n"></div>
                    <ul id="slideshow">
                        <li class="slide" style="background-image: url('./immagini/no.png')"></li>
                        <li class="slide" style="background-image: url('./immagini/fcp.jpg')"></li>
                        <li class="slide" style="background-image: url('./immagini/plague.png')"></li>
                        <li class="slide" style="background-image: url('./immagini/pulse.jpg')"></li>
                        <li class="slide" style="background-image: url('./immagini/skyrim.jpg')"></li>
                    </ul>
            </div>
            
            
            <div id="gallerysecondaria">
                <div class="dot active" data-slide="0"></div>
                <div class="dot" data-slide="1"></div>
                <div class="dot" data-slide="2"></div>
                <div class="dot" data-slide="3"></div>
                <div class="dot" data-slide="4"></div>
            </div>
       </section>