
$(document).ready(
    function() {

        var slideshowReady = false;
        var current = 0;

        var heroGallery = $( '#herogallery' );
        var slideshow = $( '#slideshow' );
        var slides = slideshow.find( '.slide' );

        var dots = heroGallery.find( '.dot' );
        var navs = heroGallery.find( '.gallerynav' );
        var slidesCounter = slides.length;
        var slideshowInterval;
        var viewPortWidth;
            
        var resizeEnd;

        var init = function() {

            setUpSlideshow();
            initSlideshow();
            
            navs.on('click', navClickHandler );
            dots.on('click', dotClickHandler );

            function windowResized(){
                setUpSlideshow();
                playSlideshow();
            }
            
            window.onresize = function() {
                if( this.slideshowInterval ) {
                    this.stopSlideshow();
                }
                clearTimeout(resizeEnd);
                resizeEnd = setTimeout(windowResized, 100);
            };

            initModal();

        }();
        
        $('.movie-info').on('click',function(){
            var movieId = $(this).data('movie-id'); //data per quando usi gli attributi ovvero quelli che dai tu
            console.log(movieId);
            $('#myModal').find('.movie-info-box').hide(); //Hide() è una funzione di Jquery che applica display none
            $('#myModal').find('#movie-info-' + movieId).show();
            $('#myModal').modal('show');
        });
        /*
        function initModal() {
            $('#login-btn, #modal-close').on('click', toggleModal);
        } */
        
        function initModal() {
            $('#login-btn').on( 'click', openModal );
            $('#modal-close,#modal-overlay').on( 'click', closeModal );
        }


        function openModal() {
            $('#modal-overlay').addClass('open');
        }

        function closeModal() {
            $('#modal-overlay').removeClass('open');
        }
        /*
        function toggleModal() {
            $('#modal-overlay').toggleClass('open');


            /*
            if( $('#modal-overlay').hasClass('open')) {
                
                $('#modal-overlay').removeClass('open');
            } else{
                $('#modal-overlay').addClass('open');
            }
        } */

        function setUpSlideshow() {

            viewPortWidth = $(window).width();

            var galleryWidth = ( viewPortWidth * slidesCounter ) + 'px';
            slideshow.css( { width: galleryWidth } );

            slides.css( { width: viewPortWidth + 'px'} );

            console.log( slides );
            if( !slideshowReady ) {

                /* Each primo metodo
                slides.each( function( i, obj, color ) {
                    console.log( i, $(obj) );
                } );*/

                /*
                // Each metodo semplice
                slides.each( function() {
                    console.log( $(this) );
                } );*/

                slides.find( '.slide' ).on( 'mouseenter', stopSlideshow);
                slides.find( '.slide' ).on( 'mouseleave', playSlideshow);
                slideshowReady = true;
            }

            var newOffset = viewPortWidth * current;
            // slideshow.style.marginLeft = -newOffset + 'px';
            // slideshow.style.left = -newOffset + 'px';
            slideshow.css({ transform:'translateX(' + -newOffset + 'px )' });

        }

        function navClickHandler( event ) {

            stopSlideshow();

            var navClicked = $(this);
            var action = navClicked.data( 'action' );

            var next = 0;

            if( action === 'p' ){

                if( current > 0 ) {
                    next = current - 1;
                } else {
                    next = slidesCounter - 1;
                }

            } else {

                if( current < slidesCounter - 1 ) {
                    next = current + 1;
                } else {
                    next = 0;
                }
            }

            goToSlide( next );
            playSlideshow();
        }

        function dotClickHandler() {
            stopSlideshow();
            var dotClicked = $(this);
            var nextSlideIndex = dotClicked.data('slide');
            var next = parseInt( nextSlideIndex, 10 );

            goToSlide( next );
            playSlideshow();
        }

        function initSlideshow() {
            playSlideshow();
        }

        function playSlideshow() {
            // console.log('playSlideshow modificato!');
            if( !slideshowInterval ){
                slideshowInterval = setInterval( handleSlideshow, 5000 );
            }
        }

        function stopSlideshow() {
            // console.log('stopSlideshow modificato!');
            clearInterval( slideshowInterval );
            slideshowInterval = undefined;
        }

        function handleSlideshow() {
            var next = current < slidesCounter - 1 ? current + 1 : 0;
            goToSlide( next );
        }


        function goToSlide( index ) {
            var currentSlide = $(slides[current]);

            // console.log( currentSlide );
            var currentDot = $('[data-slide="' + current + '"]');
            var next = index;
            var nextDot = $('[data-slide="' + next + '"]');
            var nextSlide = $(slides[next]);

            currentSlide.removeClass('active');
            currentDot.removeClass('active');
            nextSlide.addClass('active');
            nextDot.addClass('active');

            var newOffset = viewPortWidth * index;
            // slideshow.style.marginLeft = -newOffset + 'px';
            // slideshow.style.left = -newOffset + 'px';
            slideshow.css( { transform: 'translateX(' + -newOffset + 'px )' } );

            current = index;
        }
});