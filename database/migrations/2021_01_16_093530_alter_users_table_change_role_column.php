<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTableChangeRoleColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        Schema::table( 'users', function( Blueprint $table)
        {
            $table->integer('role')->change();
        });
        */
        DB::statement("ALTER TABLE users MODIFY role INT");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*
        Schema::table( 'users', function( Blueprint $table){
            $table->string('role')->change();
        } );
        */
        DB::statement("ALTER TABLE users MODIFY role VARCHAR(10)");
    }
}
