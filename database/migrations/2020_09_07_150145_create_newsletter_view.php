<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsletterView extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::statement( $this->createDetailsView() );
        DB::statement( $this->createTicketStatsView() );
        DB::statement( $this->createNewsletterUserView() );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        DB::statement( $this->dropTicketStatsView() );
        DB::statement( $this->dropDetailsView() );
        DB::statement( $this->dropNewsletterUsersView() );
    }

    private function createDetailsView() 
    {
        return <<<SQL
        CREATE VIEW `details` AS SELECT
            ud.user_id,
            DATE_FORMAT( ud.birth_date, '%d/%m/%Y' ) AS birth_date,
            ud.gender,
            ud.newsletter,
            TRUNCATE(DATEDIFF( NOW(), ud.birth_date ) / 365, 0) AS age
        FROM user_details ud;
SQL;
    }

    private function createTicketStatsView() {
        return <<<SQL
        CREATE VIEW `tickets_stats` AS SELECT
        user_id,
        COUNT( t.`id`) AS bought,
        AVG( s.`price` ) AS average,
        SUM( s.`price` ) AS spent
        FROM tickets t
        JOIN shows AS s ON t.show_id = s.id
        GROUP BY user_id;
SQL;
    }

    private function createNewsletterUserView() {
        return <<<SQL
        CREATE VIEW `newsletter_users` AS SELECT
        u.id AS user_id,
        u.name,
        u.email,
        d.gender,
        d.birth_date,
        d.age,
        IFNULL(ts.bought, 0) AS bought,
        COALESCE( ts.average, 0 ) as average,
        IF( ts.spent IS NULL, 0, ts.spent ) as spent,
        CASE
            WHEN d.gender = 'F' AND d.age < 50 THEN 'layout_1'
            WHEN d.gender = 'F' AND d.age > 50 THEN 'layout_2'
            WHEN d.gender = 'M' AND d.age < 50 THEN 'layout_3'
            WHEN d.gender = 'M' AND d.age > 50 THEN 'layout_4'
            ELSE 'layout_0'
        END AS mail_template
    FROM `users` u
        JOIN `details` AS d ON d.user_id = u.id AND d.newsletter = true AND u.email_verified_at IS NOT NULL
        LEFT JOIN `tickets_stats` AS ts ON ts.user_id = u.id;
SQL;
    }


    private function dropNewsletterUsersView() {
        return <<<SQL
        DROP VIEW IF EXISTS `newsletter_users`;
SQL;
    }

    private function dropDetailsView() {
        return <<<SQL
        DROP VIEW IF EXISTS `details`;
SQL;
    }

    private function dropTicketStatsView() {
        return <<<SQL
        DROP VIEW IF EXISTS `tickets_stats`;
SQL;
    }


}