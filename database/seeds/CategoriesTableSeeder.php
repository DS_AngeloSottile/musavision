<?php

use Illuminate\Database\Seeder;
use App\Models\Category;
use Carbon\Carbon;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $now = Carbon::now();
        $categories = 
        [
            ['name' => 'Animazione', 'code' => 'Animazione', 'icon_path' => '', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Commedia', 'code' => 'Commedia', 'icon_path' => '', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Musical', 'code' => 'Musical', 'icon_path' => '', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Horror', 'code' => 'Horror', 'icon_path' => '', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Thriller', 'code' => 'Thriller', 'icon_path' => '', 'created_at' => $now, 'updated_at' => $now]
        ];

        /* PRIMO METODO

        foreach ($categories as $category)
        {
            Category::insert($category);
        }

        */

        Category::insert($categories); //Insert prende come parametro un intero array e poi se la vede lui

    }
}
