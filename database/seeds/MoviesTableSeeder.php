<?php

use Illuminate\Database\Seeder;
use App\Models\Movie;
use App\Models\Category;
use Illuminate\Support\Arr;

class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        for ( $i=1; $i<=3; $i++ )  //per creare moooolti campi
        {
            $movie = [];
            $faker = Faker\Factory::create();
            $categories = Category::get()->pluck('id')->toArray(); // prendi solo gli id con pluck
            $movie_code = 'M';
            $code_padded = str_pad($i,4,'0',STR_PAD_LEFT);
            $movie =
                [
                    'name'                      => 'Movie' . $i,
                    'code'                      => $movie_code.$code_padded,
                    'duration'                  => $faker->numberBetween( 80, 240 ), //faker serve a creare dati appunto inventati, li vedi nella sua doceumtazione
                    'cover_path'                => 'immagini/fcp.jpg', // $faker->imageUrl(500, 1000, 'abstract'),  faker serve a creare dati appunto inventati, li vedi nella sua doceumtazione'cover_path'            => $faker->imageUrl(500, 1000, 'abstract'), //faker serve a creare dati appunto inventati, li vedi nella sua doceumtazione
                    'cover_filename'            => 'fcp.jpg',
                ];


            $new_movie = Movie::create($movie);

            // In questo modo gli stai passando valori random da u narray
            /*
            $movie_categories = [];
            for($cc = 0; $cc < 2; $cc++)
            {
                $index = array_rand($categories);
                if(!in_array($categories[$index]['id'], $movie_categories))
                {
                    $movie_categories[] = $categories[$index]['id']; // dentro l'array movie_categories ci sono gli id presi così $categories[$index]['id']
                }
            }

            $new_movie->categories()->sync($movie_categories);
            */

            $movie_categories = Arr::random($categories,2);
            $new_movie->categories()->sync($movie_categories);
        }
        // DB::table('movies')->insert($movies); //Table prende come paramentro il nome della tabella...... così lo fai senza le categorie devi aggiungere [] a $movie



    }
}
