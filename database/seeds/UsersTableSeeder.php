<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Carbon\Carbon;


use App\Models\User;
use App\Models\Role;
use App\Models\UserDetails;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //se vuoi cancellare le views devi mettere php artisan migrate:fresh --seed --drop-views

        $this->createAdmin();
        $faker = Faker\Factory::create('it_IT'); // forziamo faker ad usare il dizionario in Italiano

        for($i=0;$i<30;$i++)
        {
            $user_role = Role::where('code','user')->first();

            $user_data =
            [
                'name' => $faker->name,
                'email' => $faker->unique()->safeEmail(),
                'email_verified_at' => $faker->randomElement([now(),null]),
                'password' => Hash::make('password'), //così si generano le password di Laravel
                'remember_token' => Str::random(10),
                'last_login' =>  $faker->randomElement(
                                                    [
                                                        Carbon::parse($faker->dateTimeBetween(' -2 years ','now'))->format('Y-m-d-'),null
                                                    ]),
                'deleted_at' => null,
                'role' => $user_role->id
            ];

            $new_user = User::Create($user_data);
            if($new_user->email_verified_at != null)
            {
                $user_details = new UserDetails
                (
                [
                    'user_id' => $new_user->id,
                    'gender' => $faker->randomElement(['M','F']), //prende uno di questi valori con un 50% di probabilita
                    'birth_date' => $faker->dateTimeBetween(' -100 years ',' -18 years ')->format('Y-m-d-'), //la data che abbiamo sarà compresa tra i 18 e i 100 anni
                    'newsletter' => $faker->boolean(70), //percentuale del 70 %che newsletter sia vero
                ]
                );
                $new_user->details()->save($user_details); //esistono save, sync, attach, a seconda della relazione che metti, hastomany e bla bla bla
            }



            //UserDetails::create($user_details);
        }
    }

    private function createAdmin()
    {
        $admin_role = Role::where('code','admin')->first();
        $user_data =
            [
                'name' => 'Angelo',
                'email' => 'angeloemail@hotmail.it',
                'email_verified_at' => Carbon::now(),
                'password' => Hash::make('password'), //così si generano le password di Laravel
                'remember_token' => Str::random(10),
                'deleted_at' =>null,
                'role' =>$admin_role->id
            ];

            $new_user = User::Create($user_data);
            $user_details = new UserDetails
            (
            [
                'user_id' => $new_user->id,
                'gender' => 'M',
                'birth_date' => null,
                'newsletter' =>0
            ]
            );

            $new_user->details()->save($user_details); //esistono save, sync, attach, a seconda della relazione che metti, hastomany e bla bla bla
    }
}
