<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Arr;


use App\Models\Movie;
use App\Models\Show; 
use App\Models\Hall; 


class ShowTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       

        $movies = Movie::all();
        $halls = Hall::get()->pluck('id')->toArray();
        $shows = [];
        $now = Carbon::now(); //oggetto
        $show_counter = 1;


        //$now2 = date('Y-m-d H:i:s'); stringa
        // $startDate = Carbon::create(2020,3,2,0,0,0,'Europe/Rome'); //'2020-03-02 00:00:00' crea oggetto
        // $endDate = Carbon::create(2020,3,9,23,59,59,'Europe/Rome'); //'2020-03-09 23:59:59' crea oggetto
        $startDate = Carbon::parse('2020-03-02 00:00:00'); //'2020-03-02 00:00:00' crea oggetto equivalenti ma permette di fare confronti tra le date
        $endDate = Carbon::parse('2020-03-09 23:59:59'); //'2020-03-09 23:59:59' crea oggetto equivalenti ma permette di fare confronti tra le date

        //lte vuol dire se la prima data è minore dell'altra
        
        for( $date = $startDate; $date->lte($endDate); $date->addDay() ) //in pratica queste qui sono le date di riferimento e NON devono essere cambiare
        {
            foreach ($movies as $m =>$movie)
            { //SE VUOI MODIFICARE UNA DATA DEVI COPIARLA
                $dayStart = $date->copy()->setTime(17,30,0);
                $dayEnd = $date->copy()->setTime(22,0,0);
                $max_shows = ceil($dayEnd->diffInMinutes($dayStart) / $movie->duration); //floor prende l'intero piu' basso ceil prende l'intero piu' alto, arrotondare per difetto e per eccesso

                for($i=0;$i<$max_shows;$i++)
                {

                    //$random_hall =  Arr::random($halls,1)[0];
                    //settime permette di spostare avanti l'orologio della data,la data va bene però a QUESTO NUOVO ORARIO
                    $movieStart = $dayStart->copy()->addMinutes($i * $movie->duration); //questo per la data d'inizio //queste le usi per aiutarti, copiando da quelle di riferimento
                    $movieEnd = $movieStart->copy()->addMinutes($movie->duration); //queste le usi per aiutarti, copiando da quelle di riferimento
                    
                    $random_hall = $this->extractRandomHall($halls,$movieStart,$movieEnd);

                    $shows[] =
                    [
                        'code'      =>'S'.$show_counter,
                        'price'     =>10.00,
                        'start'     =>$movieStart,
                        'end'       =>$movieEnd,
                        'movie_id'  =>$movie->id,
                        'hall_id'   => $random_hall,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ];
                    $show_counter++;
                }
            }
        }
         Show::insert($shows);
    }
    
    
    private function extractRandomHall($halls, $start, $end)
    {
        $halls = Hall::get()->pluck('id')->toArray();
        $found = false;
        $hall_id = 0;
        $cycle_break = 10;

        while(!$found )
        {
            $random_hall_id =  Arr::random($halls,1)[0];
            $hall_empty = Show::where('hall_id',$random_hall_id)
                                ->where(function($query) use($start,$end)
                                {
                                    $query  ->whereBetween('start', [$start, $end]) //il where sarebbe, nome colonna e poi il valore 
                                            ->orWhereBetween('end', [$start, $end]);
                                }
                                )->get();
            
            if($hall_empty->isEmpty())
            {
                $found = true;

                $hall_id = $random_hall_id;
            }
            
        }

        return $hall_id;
    }
    
    
    
}
