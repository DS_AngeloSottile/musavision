<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon; //per ogni seeder hai aggiunto queste, ma questa solo se devi mettere dati fake
use App\Models\Hall; //per ogni seeder hai aggiunto queste

class HallsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $now = Carbon::now();
        $faker = Faker\Factory::create();
        $halls= [];
        for($i=1;$i<=10;$i++)
        {
            $halls[]= // così vuol dire aggiungere un elemento all'array in c++ è cosi vettore[posizione] = bla bla bla
            [
                'name' => 'Sala'.$i,
                'code' => 'S'.$i,
                'seats' => $faker->numberBetween( 80, 240 ),
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }

        Hall::insert($halls); 
    }
}
