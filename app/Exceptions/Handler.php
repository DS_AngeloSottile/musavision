<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // dd($exception); quindi, quando avrai un solito errore di laravel, sarà un dump and die di exception che ti dirà di che tipi di errore si tratti, allora ti basta fare quell'if con isntanceof e il tipo di errore ed esegurie tutte le righe di codice che desideri
        if( $exception instanceof ModelNotFoundException) //fai un elseif per catalogare i vari errori
        {
            throw new Exception('messaggio di errore'); // questo crea un'eccezzione nella cartelal vendore dentro il file log
            //return redirect()->route('home');
            //return redirect()->back()->withWarning('film insesistente'); // quelwithWarning è una funzione di ritorno messaggio che hai creato tu tramite components message
            abort(404);
        }
        return parent::render($request, $exception);
    }
}
