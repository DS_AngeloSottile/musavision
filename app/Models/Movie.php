<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    //
    protected $table = 'movies';

    protected $fillable = 
    [
       'name',
       'code',
       'short_description',
       'long_description',
       'duration',
       'trailer_mp4',
       'trailer_webm',
       'trailer_ogv',
       'cover_path',
       'cover_filename',
    ];


    public function setNameAttribute($value) //nei sei stai impostando un valore
    {
        $this->attributes['name'] = ucfirst($value);
    }
    
    public function categories()
    {
        return $this->belongsToMany('App\Models\Category','movies_categories','movie_id','category_id');
    }

    public function shows()
    {
        return $this->hasMany('App\Models\Show');
    }
}
