<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = 'categories';

    protected $fillable = 
    [
       'name',
       'code',
       'icon_path',
    ];

    
    public function movies()
    {
        return $this->belongsToMany('App\Models\Movie','movies_categories','category_id','movie_id');
    }
}
