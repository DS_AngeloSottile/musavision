<?php

namespace App\Models;

use app\Events\UserUpdated;
use app\Events\UserDeleted;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes; //lo hai aggiunto tu per implementare il soft deleting e poi devi fare la migrazione per aggiungere la colonna
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail // hai messo u implements MustVerifyEmail per riuscire a mandare un email
{
    use Notifiable; // se c'è questo trait puoi inviargli le email
    use SoftDeletes; //lo hai aggiunto tu per implementare il soft deleting e poi devi fare la migrazione per aggiungere la colonna

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role','last_login'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */

     protected $with =
     [
         'details',
     ];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function details()
    {
        return $this->hasOne('App\Models\UserDetails');
    }

    public function userRole()
    {
        return $this->belongsTo('App\Models\Role','role','id');
    }

    public function scopeActive($query)
    {
        return $query->where('email_verified_at','<>', null );
    }

    public function scopeNotactive($query)
    {
        return $query->where('email_verified_at', null );
    }

    public function scopeStatus($query,$status) // il primo parametro è già implementato. Gli scope servono per richiamare le query, in modo da non doverle scrivere piu' volte
    {
        if($status === true)
        {
            return $query->whereNotNull('email_verified_at' ); // $query->where('email_verified_at','<>', null );
        }
        else
        {
            return $query->whereNull('email_verified_at');
        }
    }
    public function hasRole( $role) // questo serve solo per i gate, devi implementare questo, poi quello i nauthserviceproviders e poi il middleware in web routes
    {
        return $this->userRole->code === $role;
    }

    /*
    protected $dispatchesEvents =
    [
        'updated'       => UserUpdated::class,
        'deleted'       => UserDeleted::class
    ]; */
}
