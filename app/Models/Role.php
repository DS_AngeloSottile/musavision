<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    protected $table = 'roles';

    protected $fillable =
    [
       'label',
       'code',
    ];

    public function userRole()
    {
        return $this->hasMany('App\Models\User','role','id');
    }
}
