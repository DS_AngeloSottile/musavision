<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class UserDetails extends Model
{
    //
    protected $table = 'user_details';

    protected $fillable =
    [
        'user_id',
        'gender',
        'birth_date',
        'newsletter',
    ];

    public function getNewsletterAttribute( $value) // in pratica questo serve a mascherare il nome e i valori degli attributi e se te lo stai chiedendo, NO, LA FUNZIONA NON VIENE RICHIAMATA, IL VALUE VIENE IGNIETTATO DAL MODELLO
    {
        if($value)
        {
            return 'SI';
        }
        else
        {
            return 'N0';
        }
    }

    public function getBirthDateAttribute()
    {
        return 'B';
    }


    public function getAgeAttribute() // Nei get stai accendendo ad una proprietà e quindi ritorni un valore
    {                                       // ->age    ->diffInYears //equivalenti
        return Carbon::parse($this->birth_date)
                    ->diff(Carbon::now()) //così eveti di importare la classe con use Carbon Carbon //queste 2 sono equivalenti
                    ->format('%y anni %m mesi %d giorni');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}
