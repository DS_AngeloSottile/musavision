<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Show extends Model
{
    //
    protected $table = 'shows';

    protected $fillable = 
    [
       'code',
       'price',
       'start',
       'end',
       'movie_id',
       'hall_id',
    ];

    // Così fai un join 1 ad 1
    public function movie()
    {
        return $this->belongsTo('App\Models\Movie');
    }

    public function hall()
    {
        return $this->belongsTo('App\Models\Hall');
    }
}
