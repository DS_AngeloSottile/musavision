<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hall extends Model
{
    //
    protected $table = 'halls';

    protected $fillable = 
    [
       'name',
       'code',
       'seats',
    ];


    public function shows()
    {
        return $this->hasMany('App\Models\Show');
    }

}
