<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function __construct()
    {
        $this->middleware('verified');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home',['pageTitle' =>'Homepage'] ); //extends loyaout app DENTRO VIEWS
    }

    public function test( $a, $b, $c = null ) //null perchè quel paramentro è opzionale
    {
        echo $a .'<br>';
        echo $b .'<br>';
        echo $c .'<br>';
    }

}
