<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; // per usare le classi del DB ho messo questa
use App\Http\Controllers\Controller;
use App\Models\User;

class AdminUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        /*
        $users = User::get(); // User::with('details')->get();details è il nome della funzione, poich+ in quella funzione è presente la relzione, allora puoi interrogare un singolo utente con la sua relazione
        foreach($users as $u=> $user)
        {
           echo  $user->name . '||' . $user->details->newsletter. '||' . $user->details->birth_date . '||' . $user->details->age . ' <br><br>'; //quei getaatribute in userdetails modificano il valore di quel campo
        }


        $movies = Movie::with('categories')->get();

        foreach($movies as $movie)
        {
            echo $movie->name . '<br>';
            foreach($movie->categories as $category)
            {
                echo $category->name . '<br>';
            }

            echo '-----------------------' . '<br>';
        }

       // $users = User::active()->get(); // <> questo sta per diverso, quindi dove email:verified_at è diverso da null
        $users = User::get();  // puoi mettere questo per prendere quelli eliminati User::onlyTrashed(true)->get(); in realtà rimaranno nel database con eliminated_at

        foreach($users as $user)
        {
            echo $user->name . '<br>';
        } */

        // $querystring = request()->query(); visualizza i parametri aggiuntivi sulla barra del browser
        $queryString = request()->query();
        $params = [];

        if(isset($queryString['sort']))
        {
            $activeUsers = User::orderBy('name',$queryString['sort'])->paginate(5); //orderBy() prende 2 parametri, il primo è il campo dove fare il sort(il name) il secondo è il posto dove si trova il valore del sort(asc o desc)
            $inactiveUsers =User::orderBy('name',$queryString['sort'])->onlyTrashed()->paginate(5);

            $params['sort'] = $queryString['sort'];
        }
        else
        {
            $activeUsers = User::paginate(5); //User::get(),
            $inactiveUsers =User::onlyTrashed()->paginate(5); //User::onlyTrashed()->get(),
        }

        $users =
        [ //withtrashed per prenderli tutti
            'active' => $activeUsers,
            'not_active' => $inactiveUsers,
        ];

        if(isset($queryString['tab']))
        {
            $current_tab = $queryString['tab'];
        }
        else
        {
            $current_tab = 1;
        }

        return view('admin.users.list',compact('users','current_tab','params'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail( $id );

        return view( 'admin.users.details', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request, $id)
    {

        $action = $request->get('delete_action'); //prendi sono delete action dall'array della request
        $user = User::withTrashed()->findOrFail($id); // in questo modo verranno presi anche gli utenti già cestinati


        if($action === "d")
        {
            /* IN QUESTO MODO NON FUNZIONA PERCHE' $id SI TROVA DENTRO UNA FUNZIONE, L'UNICO MODO PER FAR FUNZIONARE LE VARIABILI ESTERNE DENTRO UNA FUNZIONE E' USARE USE
            DB::transaction( function()
            {
                DB::table( 'user_details' )->where( [ 'user_id', $id ])->delete();
            });
            */


            /*  PRIMO METODO
            $deleted = DB::transaction( function( ) use( $id )
            {   // START TRANSITION  ...................... COMMIT
                DB::table( 'user_details' )->where( 'user_id',  $id )->delete();
                return DB::table( 'users' )->where( 'id', $id )->delete();

            });
             PRIMO METODO */


            // SECONDO METODO
            $deleted = DB::transaction( function( ) use( $user )  // se elimini un utente rimarranno i suoi dettagli, le transaction ti aiutano a non sporcare il database
            {
                $user->details()->forcedelete();
                return $user->forcedelete(); //Forcedelete invece elimina definitivamente
            });
            // SECONDO METODO

        }
        else
        {
            $deleted = $user->delete(); //visto che hai implementato il soft deleting, questa funzione "delete()" non cancellare il tuo utente, ma eseguirà una query di UPDATE modificando il campo "deleted_at" da null alla data delal cancellazione
        }


        if($deleted)
        {
            session()->flash('success',__('labels.delete_form.delete_user_form_success'));
            return redirect()->route('admin.user.index');
        }
        else
        {
            session()->flash('error',__('labels.delete_form.user_not_found'));
            return redirect()->route('admin.user.index');
        }
    }

    public function restore($id)
    {

        $user = User::withTrashed()->findOrFail($id);

        $restored = $user->restore();

        if($restored)
        {
            session()->flash('success', __('label.users.restore_success') );
            return redirect()->route('admin.user.index');
        }
        else
        {
            session()->flash('error', __('label.users.restore_error') );
            return redirect()->route('admin.user.index');
        }
    }
}
