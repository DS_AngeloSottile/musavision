<?php

namespace App\Http\Controllers\Admin; //Hai messo Admin qui è dove si trova la classe


use App\Http\Controllers\Controller; //L'hai inserita tu
use Illuminate\Http\Request;
use App\Models\Movie; // Hai messo Movie dove deve trovare la classe con le colonne
use App\Models\Category; // Hai messo Category dove deve trovare la classe con le colonne
use App\Models\Show; // Hai messo Show dove deve trovare la classe con le colonne
use Validator; //Hai messo Validator per importare questa classe
use App\Http\Requests\CreateMovieRequest; //l'hai messa tu per includere le rules
use App\Http\Requests\EditMovieRequest; //l'hai messa tu per includere le rules
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;

class AdminMoviesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // withCount aggiunge un'altra proprietà chiamata categories_count e la trovi in list.blade.php
        $movies = Movie::withCount('categories')->with('categories')->get(); //Cosi non funziona, devi importare utilizzando use, che vedi sopra
        //$movie2 = Movie::where('name','Frozen')->get();
        return view('admin.movies.list',compact('movies',)); //compact per passare varaibili da una pagina all'altra
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         // \App::setlocale('it'); // così và a leggere la cartella it da solo

        /*SECONDO METODO
            $categories_select_test = $categories->pluck('name','id'); //pluck funziona solo con le collection, devi mettere i campi che vuoi estrarre
        */

         $categories = Category::all();
         /* Se vuoi prendere una sola cosa
         $categories_select = [];
         foreach( $categories as $category)
         {
             $categories_select[$category->id] = $category->name;
         }
         */

         // Se vuoi prenderne di piu
         $categories_select = $categories->pluck('name','id');
        return view('admin.movies.create',compact('categories_select'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( CreateMovieRequest $request) // hai messo tu create movie request così i valori arrivano da questa pagina, ovviamengte devi includeral su con use
    {
        /*
        $rules =
        [
            'name'          => 'required|max:255',  // quello che vedi dopo | lo trovi nella documentazione di laravel validitations
            'code'          => 'required|max:100|unique:movies,code', //quell'unique vuol dire che il valore di code lo va a controllare nella tabella movies nel campo code
            'duration'      => 'required|numeric',
            'cover_path'    => 'required|max:255',
        ];

        $validation_message =
        [
            'required' => 'The :attribute field is required.!!!!!!!!!', //dentro english, validation, in pratica quelle stringhe NOON DEVI modificarle,ma se vuoi farlo devi fare così
        ];

        $validator = Validator::make($request->all(),$rules,$validation_message); // importa validator con use
        if($validator->fails())
        {
            return redirect()
                            ->route('movies.create')
                            ->withErrors($validator) //with vuol dire allega, ERRORS è il nome della variabile che contiene validator
                            ->withInput(); // in questo modo se sbagli una compilazione di form non vai indietro ma rimani nel form, in modo da non perdere i campi che hai immesso
        } */
        //$validatedData = $request->validate($rules,$validation_message);

        $insert_data = $request->except(['_token','categories','cover_path']); // le categorie ora sono passate come array
        $categories =  $request->get('categories'); // e quindi devvi prenderle separatamente


        $insert_data['cover_path'] ='';
        $insert_data['cover_filename'] ='';

        $new_movie = Movie::create($insert_data,); //il metodo create è già incluso $request->all()

        if($request->hasFile('cover_path'))
        {
            $file = $request->file('cover_path');
            $filename = time() . '.' . $file->getClientOriginalExtension();
            $uploaded_file = $file->storeAs('immagini/' .  $new_movie->id . '/thumbs' ,$filename,'public'); //lui prende il file e lo copia nella cartella con questo filename

            $new_movie->cover_path =$uploaded_file;
            $new_movie->cover_filename =$filename;
            $new_movie->save();

        }

        //$new_movie->categories()->attach($categories);
        $new_movie->categories()->sync($categories);
        //$new_movie->categories()->detach($categories);

        if(!$new_movie)
        {   //primo parametro la chiave del valore,probabilmente il nome, secondo parametro il contenuto della variabile
            //L\'amministratore quel "\" è un escape se lo inserisci è possibile inserire l'apostrofo come parte di una stringa
            $errors =
            [
                'error_code' => '',
                'error_message' => 'Non è stato possibile creare il film,contatta l\'Amministratore.'
            ];
            session()->flash('error',$errors); //in questo modo mandi un messaggio d'errore indietro passato tramite una variabile flash
            return redirect()->route('admin.movies.create');
        }
        else
        {
            session()->flash('success',__('labels.create_form.create_movie_form_success'));
            return redirect()->route('admin.movies.index'); //se il film è stato creato va su questa pagina, un redirect, prende il name nella route:list
        }

        // dd($validatedData->all()); // ALL() sarebbe tutto quello che ritorna tramite post
        // echo ('salva');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /* TUTTI I FILM DI ANIMAZIONE(1)
        $category = Category::with('movies')->findOrFail(1);
        dd($category);
        */

        $shows =Show::with( //se ne devi mettere di piu' prende un array con quei metodi
            [
                'hall',
                'movie.categories.movies', //navighi le funzioni
            ])->where('price', '=', 10.00)->get(); //prende tutti gli show ? iniziati dopo ora ? e uguali a 10 di  ->where('start', '>', now())


        Log::emergency('Log:emergency');
        $movie = Movie::with('categories')->findOrFail($id); //categories è la funzione dentro il modello movie
        return view('admin.movies.details',compact('movie'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $categories = Category::all();

        //Select * FROM movies where id = $id , questo è quello che fa find
        $movie = Movie::with('categories')->find($id); //findOrFail // passi il movie(cioè qull'id con le sue categorie)


        if( $movie )
        {
            return view('admin.movies.edit',compact('movie','categories'));
        }
        else
        {
            echo "Film non presente nella base dati";
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditMovieRequest $request, $id)
    {


        $input_request = $request->all();

        /* PRIMO METODO
        $data =
        [
            'name' => strtoupper($input_request['name']),
            'duration' => strtoupper($input_request['duration']),
            'cover_path' => strtoupper($input_request['cover_path']),
        ];
        $updated = Movie::where('id', $id)->update($data);
        */


        $movie = Movie::find($id);
        if($movie)
        {
            $movie->name = strtoupper($input_request['name']);
            $movie->duration = $input_request['duration'];
            $movie->cover_path = $input_request['cover_path'];

            //save ritorna true false
            $updated = $movie->save();

            if($updated)
            {
                $movie->categories()->sync($input_request['categories']);
                session()->flash('success',__('labels.edit_form.edit_movie_form_success'));
                return redirect()->route('admin.movies.index');
            }
            else
            {
                session()->flash('error',__('labels.edit_form.edit_movie_form_error'));
                return redirect()->route('admin.movies.index');
            }

        }
        else
        {
            session()->flash('error',__('labels.edit_form.movie_not_found'));
            return redirect()->route('admin.movies.index');
        }



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //PRIMO METODO  $delete = Movie::where('id', $id)->delete();

        $movie = Movie::find($id);
        if($movie)
        {
            $deleted = $movie->delete();
            session()->flash('success',__('labels.delete_form.delete_movie_form_success'));
            return redirect()->route('admin.movies.index');
        }
        else
        {
            session()->flash('error',__('labels.delete_form.movie_not_found'));
            return redirect()->route('admin.movies.index');
        }
    }
}
