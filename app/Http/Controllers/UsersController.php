<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use illuminate\Support\Facades\Auth;

class UsersController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

     /* Non puoi accedere alla rotta se l'utente è sloggato
     public function __construct()
     {
         $this->middleware('auth');
     }
     */

     public function profile()
     {
         $user = Auth::user(); //per vedere se sei loggato

         return view('users.profile',compact('user'));
     }

     public function profileUpdate(Request $request)
     {
        $new_name = $request->get('name');
        if($new_name)
        {
            $user = Auth::user();
            $user->update(['name' => $new_name]);
            return view('users.profile',compact('user'));
        }
        else
        {
            dd('errore');
        }
     }

     public function deleteProfile(Request $request)
     {
        $user = Auth::user();
        $user->delete();

        auth()->logout();
        return redirect (route('login'));

     }

}
