<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditMovieRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|max:255',  
            //'code'          => 'required|max:100|unique:movies,code,'.$this->movie, //i paramentri di unique: tabella,colonna
            'duration'      => 'required|numeric',
            'cover_path'    => 'required|max:255',
            'categories'        => 'required|array|min:1',
        ];
    }
}
