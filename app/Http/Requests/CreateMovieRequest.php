<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateMovieRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [ //se nella cover_path metti file, allora min,max e mimes servono, se metti image non servono, image ha un parametro dimension che prende cose come min-width,min-height, guarda la documentazione
            'name'              => 'required|max:255',  
            'code'              => 'required|max:100|unique:movies,code', 
            'duration'          => 'required|numeric',
            'cover_path'        => 'required|image|max:2048', //agiungi file per i file, min e max diventano automaticamente la dimensione del file, mimes per i file che vuoi mettere
            'categories'        => 'required|array|min:1',
            // 'category_id'       => 'required|numeric|exists:categories,id', //se l'id che stai passando è un numero esistente della tabella categories
        ];
    }

    public function messages()
    {
       return 
        [
            'required' => 'The :attribute field is required.!!!!!!!!!' //dentro english, validation, in pratica quelle stringhe NOON DEVI modificarle,ma se vuoi farlo devi fare così
        ];
    }
}
