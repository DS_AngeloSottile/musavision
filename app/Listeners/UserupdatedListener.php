<?php

namespace App\Listeners;

use App\Events\UserUpdated;
use App\Observers\UserObserver;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserupdatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() // per farlo funzionare devi istanziarlo nella acrtella providers eventserviceprovider
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserUpdated $event)
    {
        dd($event->user);
    }
}
