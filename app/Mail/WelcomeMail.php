<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WelcomeMail extends Mailable
{
    protected $title;
    protected $content;
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($title,$content)
    {
        $this->title    = $title;
        $this->content  = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.simple',
        [
            'title'     => $this->title,
            'content'   => $this->content
        ])
        ->attachData(public_path('storage/immagini/fcp.jpg'),'nome-file');
    }
}
