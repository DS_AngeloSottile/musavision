<?php

namespace App\Providers;

use App\Models\User;
use App\Observers\UserObserver;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Schema\Builder; //lo hai aggiunto tu, se hai problemi con il database metti questo. Sono quelle cose che ti vengono chieste quando crei una tabella

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
         Builder::defaultStringLength(191); //lo hai aggiunto tu, se hai problemi con il database metti questo. Sono quelle cose che ti vengono chieste quando crei una tabella
    }
}
