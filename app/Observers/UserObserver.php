<?php

namespace App\Observers; //PER FAR FUNZIONARE L'OBSERVER DEVI METTERLO IN APPSERVICESPROVIDER DENTRO LA CARTELLA PROVIDER, il boot

use App\Models\User;
use app\Events\UserUpdated;
use app\Events\UserDeleted;
use App\Notifications\UserUpdateNotifications;
use App\Notifications\UserDeletedNotifications;

class UserObserver
{
    /**
     * Handle the user "created" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function created(User $user)
    {
        //
    }

    /**
     * Handle the user "updated" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function updated(User $user)
    {
        echo 'qua';
        $user->notify(new UserUpdateNotifications());
    }

    /**
     * Handle the user "deleted" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        $user->notify(new UserDeletedNotifications());
    }

    /**
     * Handle the user "restored" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the user "force deleted" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }
}
