<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class Test extends Notification
{ //questa classe serve per inviare le email, così la crei  php artisan make:notification Test e così la mandi
   // $user->notify(new Test($messaggio)); // in questo modo gl istai amndando un email
    use Queueable;

    protected $message;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($message ='Default message')
    {
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)

                    ->greeting('CIAO')
                    ->success() //cambia il bottone in verde
                    //>error() //cambia il bottone in rosso
                    ->line('The introduction to the notification.') // contenuto dell'email
                    ->line($this->message)
                    ->action('Notification Action', url('/')) // contenuto dell'email
                    ->line('Thank you for using our application!'); // contenuto dell'email
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
