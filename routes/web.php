<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsersController;


Auth::routes(
    [ //php artisan route:clear
        'verify'          => true, //attiva l'email di verifica se clicci nell'email che arriva su mailtrap l'account di verifica risulterà attivo su SQL
        // 'register'       => true, //attiva la registrazione
        // 'reset'          => false,
    ] );

Route::get('/', 'SiteController@index')->name('home');

Route::get('/contact-us', 'SiteController@contacts')->name('contacs-us');

Route::get('/entity/{id}/{name}/{filter?}', 'SiteController@test'); // scrivili nella barra del browser il punto interrogativo sta come opzionale

// Route::resource('movies', 'Admin\AdminMoviesController'); //O cosi' nel secondo parametro è già impostata la cartella controllers

Route::prefix('admin-movies-managment')->namespace('Admin')->name('admin.')->middleware('can:can-admin')->group( function ()
{
    Route::resource('movies', 'AdminMoviesController'); // in AdminMoviesManagmetn hai le funzioni pubblice, create,show,destroy. Il metodo resource crea quelle rotte
});


Route::prefix('admin-users-managment')->namespace('Admin')->name('admin.')->middleware('can:can-admin')->group( function () // per poter utilizzare can-admin devi andare in authserviceprovider nella cartella providers
{
    Route::resource('user', 'AdminUserController')->except(['create','store']);
    Route::get('user/{user}/restore','AdminUserController@restore')->name('user.restore');
});


Route::get('/test', 'TestController@index')->middleware('verified');

Route::get('/profile', 'UsersController@profile')->name('profile');
Route::patch('/update-profile', 'UsersController@profileUpdate')->name('update-profile');
Route::delete('/delete-profile', 'UsersController@deleteProfile')->name('delete-profile');
